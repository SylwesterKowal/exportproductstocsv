<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\ExportProductsToCsv\Model\Config\Source;

class Attributes implements \Magento\Framework\Option\ArrayInterface
{
    /** @var array */
    private $items;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory
    )
    {
        $this->collectionFactory = $collectionFactory;

    }

    public function toOptionArray()
    {
        if (is_null($this->items)) {
            $this->items = $this->getOptions();
        }

        return $this->items;
    }

    public function toArray()
    {
        $attr = [];
        if($items = $this->toOptionArray()) {
            foreach ($items as $item){
                $attr[$item['value']] = $item['label'];
            }
            return $attr; //['sku' => __('sku'), 'description' => __('description')];
        }
    }

    /**
     * @return array
     */
    private function getOptions()
    {
        $items = [];
        foreach ($this->getAttributes() as $attribute) {

                $items[] = [
                    'label' => $attribute->getStoreLabel(), 'value' => $attribute->getName(),
                ];

        }
        return $items;
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection
     */
    private function getCollection()
    {
        return $this->collectionFactory->create();
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Eav\Attribute[]|\Magento\Framework\DataObject[]
     */
    public function getAttributes()
    {

        $collection = $this->getCollection();
        return $collection->getItems();
    }
}

