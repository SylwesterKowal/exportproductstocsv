<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\ExportProductsToCsv\Controller\Adminhtml\Exportcsv;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

use Magento\Framework\Controller\ResultInterface;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Filesystem\Io\File;
use Kowal\ExportProductsToCsv\Helper\Data;

class Index extends \Magento\Backend\App\Action
{
    protected $fileName;
    protected $path_to_file;


    /**
     * @param Context $context
     * @param Data $helperData
     * @param Filter $filter
     * @param CollectionFactory $prodCollFactory
     * @param ProductRepositoryInterface $productRepository
     * @param DirectoryList $directoryList
     * @param FileFactory $fileFactory
     * @param RawFactory $resultRawFactory
     * @param File $file
     */
    public function __construct(
        Context                    $context,
        Data                       $helperData,
        Filter                     $filter,
        CollectionFactory          $prodCollFactory,
        ProductRepositoryInterface $productRepository,
        DirectoryList              $directoryList,
        FileFactory                $fileFactory,
        RawFactory                 $resultRawFactory,
        File                       $file
    )
    {

        $this->helperData = $helperData;
        $this->filter = $filter;
        $this->prodCollFactory = $prodCollFactory;
        $this->productRepository = $productRepository;
        $this->directoryList = $directoryList;
        $this->fileFactory = $fileFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->file = $file;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return ResultInterface
     */
    public function execute()
    {

        $collection = $this->filter->getCollection($this->prodCollFactory->create());
        $list = [];
        $list[] = $this->addHeaders();
        foreach ($collection->getAllIds() as $productId) {
            $productDataObject = $this->productRepository->getById($productId);

            $list[] = $this->addRow($productDataObject);
        }


        $resultRaw = $this->downloadFile($list);
        $this->messageManager->addSuccess(__('Export %1 products.', $collection->getSize()));
        return $resultRaw;
    }

    public function addHeaders()
    {
        $cols = [];
        if ($attr = $this->helperData->getGeneralCfg('attributes')) {
            $cols = explode(",", $attr);
        }
        array_push($cols, 'product_type');
        return $cols;
    }

    public function addRow($product)
    {
        $row = [];
        if ($attr = $this->helperData->getGeneralCfg('attributes')) {
            $columns = explode(",", $attr);
            foreach ($columns as $col) {
                $getName = "get" . ucfirst($col);
                if($label = $product->getResource()->getAttribute($col)->getFrontend()->getValue($product)){
                    if (is_array($label)) {
                        $value_ = implode("|", $label);
                    } else {
                        $value_ = $label;
                    }
                }else {
                    $value = $product->$getName();
                    if (is_array($value)) {
                        $value_ = implode("|", $value);
                    } else {
                        $value_ = $value;
                    }
                }
                array_push($row, $value_);
            }
            array_push($row, $product->getTypeId());
        }
        return $row;
    }

    private function downloadFile($pm)
    {
        $this->getFileName()->saveArrayToCsv($pm);


        $this->fileFactory->create(
            $this->fileName,
            file_get_contents($this->path_to_file),
            \Magento\Framework\App\Filesystem\DirectoryList::MEDIA
        );
        $resultRaw = $this->resultRawFactory->create();
        return $resultRaw;
    }

    private function getFileName()
    {
        $this->var = $this->directoryList->getPath('var');

        if (!file_exists($this->var . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'pm')) {
            $this->file->mkdir($this->var . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'pm', 0775);
        }
        $this->fileName = 'pm_export_' . date('Ymd_His') . '.csv';
        $this->path_to_file = $this->var . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'pm' . DIRECTORY_SEPARATOR . $this->fileName;
        return $this;
    }

    private function saveArrayToCsv($array)
    {
        $fp = fopen($this->path_to_file, 'w');
        foreach ($array as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);
    }
}